# ORM

Simple ORM lib. Tested with HSQLDB.

## Config and start

Init lib:

```
OrmService.init(JDBCDriver.class.getName(), "jdbc:hsqldb:file:data/db", "user", "pass");
```

Create/update schema from resource file:

```
String commandSeparator = "GO";
OrmService.createSchema(Resources.toString(Resources.getResource("sql/schema.sql"), Charsets.UTF_8), commandSeparator);
```

POJO:

```
@Table("Books")
public class Book {

    @Sequence
    private int id;
    @Column("title")
    private String name;
    @Ignore
    private String author;

}
```

Repo class:

```
public class BookRepo extends Repository<Book> {

    private static BookRepo instance = new BookRepo();

    private BookRepo() {
        super(Book.class);
    }
    
    public BookRepo getInstance() {
        return instance;
    }
    
}
```

### Usage

```
List<Book> books = BookRepo.getInstance().findAll();
Book book = BookRepo.getInstance().findOne(16);
int id = BookRepo.getInstance().insert(new Book(...));
List<Book> books = BookRepo.getInstance().findBy("title", "ABC");
...
```
