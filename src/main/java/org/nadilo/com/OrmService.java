package org.nadilo.com;

import org.nadilo.com.util.RowHandler;

import java.sql.*;
import java.util.List;
import java.util.function.Consumer;

public class OrmService {

    private static String driverName;
    private static String url;
    private static String username;
    private static String password;

    private static Connection connection;
    private static Consumer<Exception> onException;
    private static boolean doThrows;

    public static void init(String driverName, String url, String username, String password) {
        OrmService.driverName = driverName;
        OrmService.url = url;
        OrmService.username = username;
        OrmService.password = password;
    }

    public static void setOnException(Consumer<Exception> onException, boolean doThrows) {
        OrmService.onException = onException;
        OrmService.doThrows = doThrows;
    }

    public static Connection getConnection() {
        try {
            if (connection != null && !connection.isClosed())
                return connection;

            Class.forName(driverName);

            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(true);

            return connection;

        } catch (Exception e) {
            handleException(e);
            throw new RuntimeException(e);
        }
    }

    public static void runTransaction(Consumer<Connection> connectionConsumer) {
        Connection connection = getConnection();

        try {
            connection.setAutoCommit(false);
            connectionConsumer.accept(connection);
            connection.commit();
        } catch (Exception e) {
            try {
                connection.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                handleException(e);
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void createSchema(String sql, String commandSeparator) {
        for (String command : sql.split(commandSeparator)) {
            try {
                OrmService.execute(command.trim());
            } catch (Exception e) {
                if (isExceptionIgnored(e))
                    continue;

                handleException(e);
            }
        }
    }

    private static boolean isExceptionIgnored(Exception e) {
        return e.getMessage().toLowerCase().contains("object name already exists")
                || e.getMessage().toLowerCase().contains("constraint already exists")
                || e.getMessage().toLowerCase().contains("unique constraint or index violation");
    }

    public static void execute(String sql) {
        try (Statement statement = getConnection().createStatement()) {
            statement.execute(sql);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public static int executePrepared(String sql, Object... params) {
        try (PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            applyParams(ps, params, false);
            ps.execute();

            try (ResultSet rs = ps.getGeneratedKeys()) {
                return rs.next() ? rs.getInt(1) : -1;
            }
        } catch (Exception e) {
            handleException(e);
            return -1;
        }
    }

    static void executeQueryInternal(String sql, Object[] params, RowHandler rowHandler) {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            applyParams(ps, params, true);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next())
                    rowHandler.processRow(rs);
            }

        } catch (Exception e) {
            handleException(e);
        }
    }

    public static void executeQuery(String sql, Object[] params, RowHandler rowHandler) {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            applyParams(ps, params, false);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next())
                    rowHandler.processRow(rs);
            }

        } catch (Exception e) {
            handleException(e);
        }
    }

    public static void executeBatch(String sql, List<Object[]> rows) {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            for (Object[] row : rows) {
                applyParams(ps, row, false);
                ps.addBatch();
            }

            ps.executeBatch();

        } catch (Exception e) {
            handleException(e);
        }
    }

    public static ResultSet getResultSet(String sql, Object[] params) {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            applyParams(ps, params, false);
            return ps.executeQuery();
        } catch (Exception e) {
            handleException(e);
            return null;
        }
    }

    public static void shutdown() {
        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("SHUTDOWN");
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private static void applyParams(PreparedStatement ps, Object[] params, boolean isInternalQuery) {
        if (params == null || params.length == 0)
            return;

        try {
            for (int i = 0; i < params.length; i ++) {
                if (isInternalQuery && params[i] == null)
                    continue;

                ps.setObject(i + 1, params[i]);
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    private static void handleException(Exception e) {
        if (onException == null)
            throw new RuntimeException(e);
        else {
            onException.accept(e);

            if (doThrows)
                throw new RuntimeException(e);
        }
    }
}
