package org.nadilo.com;

import org.nadilo.com.annotation.Column;
import org.nadilo.com.annotation.Ignore;
import org.nadilo.com.annotation.Sequence;
import org.nadilo.com.annotation.Table;
import org.nadilo.com.util.Order;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.nadilo.com.OrmService.executePrepared;
import static org.nadilo.com.OrmService.executeQueryInternal;

public abstract class Repository<T> {

    private final Class<T> klass;
    private final Field[] fields;
    private final Field sequence;

    protected final String table;
    protected final String sequenceName;

    private String insertSql;
    private String insertWithIdSql;
    private String updateSql;
    private String deleteSql;
    private String selectSql;

    public Repository(Class<T> klass) {
        this.klass = klass;

        this.fields = Arrays.stream(klass.getDeclaredFields())
                .filter(f -> !f.isAnnotationPresent(Ignore.class))
                .filter(f -> !f.getName().equals("Companion")) // Kotlin hack
                .toArray(Field[]::new);

        this.sequence = Arrays.stream(fields).filter(f -> f.isAnnotationPresent(Sequence.class)).findAny().orElse(fields[0]);

        this.table = getTableName();
        this.sequenceName = getColumnName(sequence);

        StringBuilder insertBuilder = new StringBuilder("INSERT INTO " + table + " (");
        StringBuilder insertWithIdBuilder = new StringBuilder("INSERT INTO " + table + " (");
        StringBuilder updateBuilder = new StringBuilder("UPDATE " + table + " SET ");

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);

            String column = getColumnName(fields[i]);

            insertWithIdBuilder.append(column);

            if (i != fields.length - 1)
                insertWithIdBuilder.append(", ");

            if (fields[i] == sequence)
                continue;

            insertBuilder.append(column);
            updateBuilder.append(column).append(" = ?");

            if (i != fields.length - 1) {
                insertBuilder.append(", ");
                updateBuilder.append(", ");
            }
        }

        insertBuilder.append(") VALUES (").append(IntStream.range(1, fields.length).mapToObj(i -> "?").collect(Collectors.joining(", "))).append(")");
        insertWithIdBuilder.append(") VALUES (").append(IntStream.range(0, fields.length).mapToObj(i -> "?").collect(Collectors.joining(", "))).append(")");
        updateBuilder.append(" WHERE ").append(sequenceName).append(" = ?");

        insertSql = insertBuilder.toString();
        insertWithIdSql = insertWithIdBuilder.toString();
        updateSql = updateBuilder.toString();
        deleteSql = "DELETE FROM " + table + " WHERE " + sequenceName + " IN ";
        selectSql = "SELECT * FROM " + table;
    }

    private String getTableName() {
        if (klass.isAnnotationPresent(Table.class))
            return klass.getAnnotation(Table.class).value();

        return klass.getSimpleName();
    }

    private String getColumnName(Field field) {
        if (field.isAnnotationPresent(Column.class))
            return field.getAnnotation(Column.class).value();

        return field.getName();
    }

    public T save(T t) {
        try {
            Number id = (Number) sequence.get(t);
            return id.longValue() <= 0 ? insert(t) : update(t);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(Number id) {
        executePrepared("DELETE FROM " + table + " WHERE " + sequenceName + " = ?", id);
    }

    public void delete(Collection<Number> ids) {
        String marks = ids.stream().map(id -> "?").collect(Collectors.joining(", "));
        executePrepared(deleteSql + "(" + marks + ")", ids.toArray());
    }

    public void deleteAll() {
        executePrepared("DELETE FROM " + table);
    }

    public T findOne(Number id) {
        if (id == null)
            return null;

        AtomicReference<T> tRef = new AtomicReference<>();

        executeQueryInternal(selectSql + " WHERE " + sequenceName + " = ?", new Object[]{id}, rs -> {
            T t = klass.newInstance();

            for (Field field : fields) {
                field.set(t, rs.getObject(getColumnName(field), field.getType()));
            }

            tRef.set(t);
        });

        return tRef.get();
    }

    public List<T> findAll() {
        List<T> rows = new ArrayList<>();

        executeQueryInternal(selectSql, null, rs -> {
            T row = klass.newInstance();

            for (Field field : fields) {
                field.set(row, rs.getObject(getColumnName(field), field.getType()));
            }

            rows.add(row);
        });

        return rows;
    }

    public List<T> findAllOrderBy(String[] columns, Order[] orders) {
        if (columns.length != orders.length)
            throw new RuntimeException("Unequal column and orders length");

        List<T> rows = new ArrayList<>();

        String sql = selectSql + " ORDER BY " + IntStream.range(0, columns.length)
                .mapToObj(i -> columns[i] + " " + orders[i].name())
                .collect(Collectors.joining(", "));

        executeQueryInternal(sql, null, rs -> {
            T row = klass.newInstance();

            for (Field field : fields) {
                field.set(row, rs.getObject(getColumnName(field), field.getType()));
            }

            rows.add(row);
        });

        return rows;
    }

    public List<T> findBy(String column, Object param) {
        return findBy(new String[]{column}, new Object[]{param});
    }

    public List<T> findByOrderBy(String column, Object param, String sortCol, Order order) {
        return findByOrderBy(new String[]{column}, new Object[]{param}, new String[]{sortCol}, new Order[]{order});
    }

    public List<T> findBy(String[] columns, Object[] params) {
        return findByOrderBy(columns, params, null, null);
    }

    public List<T> findByOrderBy(String[] columns, Object[] params, String[] sortCols, Order[] orders) {
        if (columns.length != params.length)
            throw new RuntimeException("Unequal column and params length");

        List<T> rows = new ArrayList<>();

        StringBuilder sql = new StringBuilder(selectSql).append(" WHERE");

        for (int i = 0; i < columns.length; i++) {
            if (i != 0)
                sql.append(" AND");

            sql.append(" ").append(columns[i]).append(params[i] != null ? " = ?" : " IS NULL");
        }

        if (orders != null && orders.length != 0) {
            sql.append(" ORDER BY ").append(
                    IntStream.range(0, sortCols.length)
                            .mapToObj(i -> sortCols[i] + " " + orders[i].name())
                            .collect(Collectors.joining(", ")));
        }

        executeQueryInternal(sql.toString(), params, rs -> {
            T row = klass.newInstance();

            for (Field field : fields) {
                field.set(row, rs.getObject(getColumnName(field), field.getType()));
            }

            rows.add(row);
        });

        return rows;
    }

    private T insert(T t) {
        try {
            Number id = executePrepared(insertSql, getParams(t, false));
            sequence.set(t, id);

            return t;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void insertWithId(T t) {
        executePrepared(insertWithIdSql, getParams(t, null));
    }

    public T update(T t) {
        executePrepared(updateSql, getParams(t, true));
        return t;
    }

    public long count() {
        AtomicLong count = new AtomicLong();
        executeQueryInternal("SELECT COUNT(*) FROM " + table, null, rs -> count.set(rs.getLong(1)));

        return count.get();
    }

    private Object[] getParams(T t, Boolean isUpdate) {
        try {
            int length = (isUpdate == null || isUpdate) ? fields.length : fields.length - 1;
            Object[] params = new Object[length];

            if (isUpdate == null)
                params[0] = sequence.get(t);

            int i = isUpdate == null ? 1 : 0;

            for (Field field : fields) {
                if (field != sequence)
                    params[i++] = field.get(t);
            }

            if (isUpdate != null && isUpdate)
                params[length - 1] = sequence.get(t);

            return params;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
