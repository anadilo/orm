package org.nadilo.com.util;

import java.sql.ResultSet;

public interface RowHandler {

    void processRow(ResultSet rs) throws Exception;

}