package org.nadilo.com;

import org.hsqldb.jdbc.JDBCDriver;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nadilo.com.model.Snippet;
import org.nadilo.com.repo.KeymapRepo;
import org.nadilo.com.repo.SnippetRepo;
import org.nadilo.com.util.Order;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RepositoryTest {

    private static SnippetRepo snippetRepo;
    private static KeymapRepo keymapRepo;

    @BeforeClass
    public static void setup() {
        OrmService.init(JDBCDriver.class.getName(), "jdbc:hsqldb:file:db/db", "", "");
        OrmService.createSchema(resourceToString("schema.sql"), "GO-EXEC");

        snippetRepo = new SnippetRepo();
        keymapRepo = new KeymapRepo();
    }

    @AfterClass
    public static void teardown() throws Exception {
        OrmService.shutdown();

        Files.walk(FileSystems.getDefault().getPath("db"))
                .map(Path::toFile)
                .forEach(File::delete);

        new File("db").delete();
    }

    private static String resourceToString(String resource) {
        try (InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(resource)) {
            return new BufferedReader(new InputStreamReader(is))
                    .lines()
                    .collect(Collectors.joining(System.lineSeparator()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testQuery() {
        assertEquals(3, snippetRepo.findAll().size());
        assertNotNull(keymapRepo.findBy("SnippetId", null));
        assertEquals(1, keymapRepo.findByOrderBy("SnippetId", null, "Id", Order.DESC).size());
    }

    @Test
    public void testSaveAndDelete() {
        Snippet s = new Snippet();
        s.setShortcut("abs");

        s = snippetRepo.save(s);
        assertEquals(4, s.getId());

        snippetRepo.delete(4);
        assertEquals(3, snippetRepo.count());
    }

    @Test
    public void testCustomQuery() {
        List<Integer> results = new ArrayList<>();

        OrmService.executeQuery(
                "SELECT * FROM Snippet WHERE Tags = ? OR (? IS NULL AND Tags IS NULL)",
                new Object[] { null, null },
                rs -> results.add(rs.getInt("Id")));

        assertEquals(1, results.size());

        results.clear();
        OrmService.executeQuery(
                "SELECT * FROM Snippet WHERE (? IS NULL OR Snippet LIKE ?)",
                new Object[] { "SELECT%", "SELECT%" },
                rs -> results.add(rs.getInt("Id")));

        assertEquals(1, results.size());
    }

}
