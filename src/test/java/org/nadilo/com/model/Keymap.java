package org.nadilo.com.model;

public class Keymap {

    private int id;
    private Integer snippetId;
    private Integer code;
    private Integer modifier;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSnippetId() {
        return snippetId;
    }

    public void setSnippetId(Integer snippetId) {
        this.snippetId = snippetId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getModifier() {
        return modifier;
    }

    public void setModifier(Integer modifier) {
        this.modifier = modifier;
    }
}
