package org.nadilo.com.model;

import java.sql.Timestamp;

public class Snippet {

    private int id;
    private String shortcut;
    private String snippet;
    private String tags;
    private boolean hidden;
    private Timestamp used;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortcut() {
        return shortcut;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Timestamp getUsed() {
        return used;
    }

    public void setUsed(Timestamp used) {
        this.used = used;
    }
}
