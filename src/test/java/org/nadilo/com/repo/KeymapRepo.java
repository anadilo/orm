package org.nadilo.com.repo;

import org.nadilo.com.Repository;
import org.nadilo.com.model.Keymap;

public class KeymapRepo extends Repository<Keymap> {

    public KeymapRepo() {
        super(Keymap.class);
    }
}
