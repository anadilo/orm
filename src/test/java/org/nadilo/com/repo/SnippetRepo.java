package org.nadilo.com.repo;

import org.nadilo.com.Repository;
import org.nadilo.com.model.Snippet;

public class SnippetRepo extends Repository<Snippet> {

    public SnippetRepo() {
        super(Snippet.class);
    }
}
